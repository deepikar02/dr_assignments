DROP PROCEDURE IF EXISTS bitwiseEncrypt;

DELIMITER |


CREATE PROCEDURE bitwiseEncrypt(IN entext varchar(50), keep_phrase bool)

BEGIN
	DECLARE str1 VARCHAR(50);
	DECLARE restr VARCHAR(50);

			IF(keep_phrase) 
			THEN
				SELECT entext INTO str1;
    			SET restr=AES_ENCRYPT(str1,'^');
    			SELECT restr as encrypt_value;
			ELSE
				SELECT entext INTO str1;
    			SET restr=AES_DECRYPT(str1,'^');
    			SELECT restr as decrypt_value;
    		END IF;
END
|

DELIMITER ;


call bitwiseEncrypt('deepika',true);