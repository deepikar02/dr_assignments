DROP PROCEDURE IF EXISTS EncryptWithKey;

DELIMITER |


CREATE PROCEDURE EncryptWithKey(IN entext varchar(50), IN keyval varchar(8))

BEGIN
	DECLARE str1 VARCHAR(50);
	DECLARE key1 VARCHAR(8);
	DECLARE restr VARCHAR(50);
	DECLARE len INT;


	IF keyval REGEXP '[A-Za-z][0-9]' && LENGTH(keyval)>=6 && LENGTH(keyval)<9 
	THEN
	
		 IF entext REGEXP '^[[:alnum:]]+$'
		 THEN
				SELECT entext,keyval INTO str1,key1;
    			SET restr=AES_ENCRYPT(str1,key1);
    			SELECT restr as encrypt_value;
  		 END IF;

  		 IF entext NOT REGEXP '^[[:alnum:]]+$'
		THEN			
				SELECT entext,keyval INTO str1,key1;
    			SET restr=AES_DECRYPT(str1,key1);
    			SELECT restr as encrypt_value;
  		 END IF;
			
	ELSE
     SELECT 'key length must be between 6-8' AS error;
	END IF;
END
|

DELIMITER ;


call EncryptWithKey('ÌÑþ‘©Ð.EúYVrWIÔ`','a1b2c3');