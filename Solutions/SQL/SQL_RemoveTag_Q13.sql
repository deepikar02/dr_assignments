# run this to drop/update the stored function
DROP FUNCTION IF EXISTS `strip_tags`;
SET GLOBAL log_bin_trust_function_creators = 1;
DELIMITER |

CREATE FUNCTION strip_tags(str text, tag text, keep_phrase bool) 
RETURNS text
 BEGIN
        DECLARE start, end INT DEFAULT 1;
        SET str = COALESCE(str, '');
    LOOP
      SET start = LOCATE(CONCAT('<', tag), str, start);
            IF (!start) THEN RETURN str; END IF;
            IF (keep_phrase) THEN
                SET end = LOCATE('>', str, start);
                IF (!end) THEN SET end = start; END IF;
                SET str = INSERT(str, start, end - start + 1, '');
                SET str = REPLACE(str, CONCAT('</', tag, '>'), '');
    		  ELSE
                SET end = LOCATE(CONCAT('</', tag, '>'),str,start);
                IF (!end) THEN 
                    SET end = LOCATE('/>',str,start); 
                    SET str = INSERT(str, start, end - start + 2, '');
                ELSE 
                    SET str = INSERT(str, start, end - start 
                       + LENGTH(CONCAT('</', tag, '>')), '');
                END IF;
            END IF;
        END LOOP;
END;
| 
DELIMITER ;

# test select to all opening <p> tags
SELECT 
    STRIP_TAGS(Address, 'p', false) 
	 AS stripped
FROM
    employee;

# run update query to replace out all <p> tags
UPDATE employee
SET 
    Address = STRIP_TAGS(Address, 'p', false);