DROP PROCEDURE IF EXISTS DistrictRank;
DELIMITER |

CREATE PROCEDURE DistrictRank( dname varchar(50), drank int)
BEGIN
 
 SELECT district_name, population
   FROM
     (SELECT district_name, population, 
                  @country_rank := IF(@current_country = district_name, @country_rank + 1, 1) AS country_rank,
                  @current_country := district_name 
       FROM tbl_district
       ORDER BY population DESC
     ) ranked
   WHERE district_name=dname and country_rank=drank;
END
|
DELIMITER ;


Call DistrictRank('satara',4);





 

	