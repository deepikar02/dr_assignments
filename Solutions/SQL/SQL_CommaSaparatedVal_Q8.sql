DROP FUNCTION IF EXISTS PersonName;
DELIMITER |

CREATE FUNCTION PersonName( dname varchar(50) )
RETURNS varchar(50)
BEGIN
  DECLARE pname varchar(50) DEFAULT '';
  SELECT GROUP_CONCAT(population) INTO pname FROM tbl_district where district_name=dname GROUP BY district_name;
  RETURN pname;
END
|
DELIMITER ;

select PersonName('satara');