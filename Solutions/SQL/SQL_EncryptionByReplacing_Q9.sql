 DROP FUNCTION IF EXISTS char_ascii2;

DELIMITER |

CREATE FUNCTION char_ascii2(str varchar(50))
RETURNS varchar(50)

BEGIN 
	DECLARE con_var varchar(50);
	DECLARE sub_var varchar(50);
	DECLARE ascii_var varchar(50);
	DECLARE ascii_var1 varchar(50);
	DECLARE result varchar(50);
	DECLARE result1 varchar(50);
	DECLARE char_var varchar(50);
	DECLARE cnt int;
	DECLARE len int;
	DECLARE comp varchar(50);
	DECLARE result_str varchar(50);
	
	SET cnt=1;
	SET len=length(str);
	
	WHILE(cnt<=len)
	DO
		SET sub_var=SUBSTRING(str,cnt,1);
		IF sub_var not REGEXP '^[[:alnum:]]+$'
		THEN 
		SET ascii_var=ASCII(sub_var);
		IF (ascii_var<128)
		THEN
			SET ascii_var1=ascii_var+128;
			SET char_var=CHAR(ascii_var1);
		ELSEIF (ascii_var>=128)
		THEN
			SET ascii_var1=ascii_var-128;
			SET char_var=CHAR(ascii_var1);
		END IF;
		
		SET result1=concat(ifnull(result1,''),char_var);
		
		ELSEIF sub_var REGEXP '^[A-Za-z]+$'
		THEN 
			SET result=
			CASE
         WHEN sub_var IN ('A') THEN 'Z'
         WHEN sub_var IN ('B') THEN 'Y'
         WHEN sub_var IN ('C') THEN 'X'
         WHEN sub_var IN ('D') THEN 'W'
         WHEN sub_var IN ('E') THEN 'V'
         WHEN sub_var IN ('F') THEN 'U'
         WHEN sub_var IN ('G') THEN 'T'
         WHEN sub_var IN ('H') THEN 'S'
         WHEN sub_var IN ('I') THEN 'R'
      	WHEN sub_var IN ('J') THEN 'Q'
      	WHEN sub_var IN ('K') THEN 'P'
      	WHEN sub_var IN ('L') THEN 'O'
      	WHEN sub_var IN ('M') THEN 'N'
      	WHEN sub_var IN ('N') THEN 'M'
      	WHEN sub_var IN ('O') THEN 'L'
      	WHEN sub_var IN ('P') THEN 'K'
      	WHEN sub_var IN ('Q') THEN 'J'
      	WHEN sub_var IN ('R') THEN 'I'
      	WHEN sub_var IN ('S') THEN 'H'
      	WHEN sub_var IN ('T') THEN 'G'
      	WHEN sub_var IN ('U') THEN 'F'
      	WHEN sub_var IN ('V') THEN 'E'
      	WHEN sub_var IN ('W') THEN 'D'
      	WHEN sub_var IN ('X') THEN 'C'
      	WHEN sub_var IN ('Y') THEN 'B' 
      	WHEN sub_var IN ('Z') THEN 'A'
      	ELSE 0
      	END;
			
			SET result1 =concat(ifnull(result1,''),result);
			
			
		ELSEIF sub_var REGEXP '^[0-9]+$'	
		THEN
		SET result=
			CASE
         WHEN sub_var IN ('9') THEN 0
         WHEN sub_var IN ('8') THEN 1
         WHEN sub_var IN ('7') THEN 2
         WHEN sub_var IN ('6') THEN 3
         WHEN sub_var IN ('5') THEN 4
         WHEN sub_var IN ('4') THEN 5
         WHEN sub_var IN ('3') THEN 6
         WHEN sub_var IN ('2') THEN 7
         WHEN sub_var IN ('1') THEN 8
      	WHEN sub_var IN ('0') THEN 9
      	ELSE 0
			END;
			
			SET result1 =concat(ifnull(result1,''),result);
			SET result_str=result1;
		END IF;
			
		SET result_str=result1;
		SET cnt=cnt+1;
	END WHILE;
	
	RETURN result_str;

END
|
DELIMITER ;
	
SELECT char_ascii2('ASDEFR@$&CF67@89');