DROP PROCEDURE IF EXISTS SplitString;

DELIMITER |

CREATE PROCEDURE SplitString(str varchar(50))
BEGIN

DROP TABLE IF EXISTS t;
CREATE TABLE t( txt text );
INSERT INTO t VALUES(str);

drop temporary table if exists temp;
create temporary table temp( val char(255) );

SET @sql = concat("INSERT INTO temp (val) VALUES ('", REPLACE(( SELECT group_concat(DISTINCT txt) AS DATA FROM t), ",", "'),('"),"');");
PREPARE stmt1 FROM @sql;
EXECUTE stmt1;
SELECT DISTINCT(val) FROM temp;

END
|
DELIMITER ;


call SplitString('zCon,Solutions,Raj,Business');