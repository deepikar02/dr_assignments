DROP PROCEDURE IF EXISTS EvenOddCol;
DELIMITER |

CREATE PROCEDURE EvenOddCol()
BEGIN
 select 
  MIN(district_id) as district_id,
  MAX(CASE district_id%2 WHEN 1 THEN district_name END) as district_name,
  MAX(district_id) as district_id,
  MAX(CASE district_id%2 WHEN 0 THEN district_name END) as district_name
FROM tbl_district
GROUP BY FLOOR((district_id+1)/2);

END
|
DELIMITER ;




Call EvenOddCol();