4) SELECT books.book_title,author.first_name,author.last_name
FROM tbl_deepika_book_authors
INNER JOIN books ON  tbl_deepika_book_authors.book_id = books.book_id
INNER JOIN author ON tbl_deepika_book_authors.author_id = author.author_id
ORDER BY books.book_title

3) SELECT books.book_title,tbl_deepika_book_sales.city,tbl_deepika_book_sales.Year_data,tbl_deepika_book_sales.quantity
FROM tbl_deepika_book_sales
INNER JOIN books ON  tbl_deepika_book_sales.book_id = books.book_id
order by books.book_title, tbl_deepika_book_sales.city, tbl_deepika_book_sales.Year_data;

2) SELECT books.book_title,tbl_deepika_book_sales.city,tbl_deepika_book_sales.quantity
FROM tbl_deepika_book_sales
INNER JOIN books ON  tbl_deepika_book_sales.book_id = books.book_id
order by books.book_title, tbl_deepika_book_sales.city;

1) SELECT tbl_deepika_book_sales.Year_data,books.book_title,tbl_deepika_book_sales.quantity
FROM tbl_deepika_book_sales
INNER JOIN books ON  tbl_deepika_book_sales.book_id = books.book_id
order by books.book_title;
