DROP PROCEDURE IF EXISTS calculateEmi;

DELIMITER |

CREATE PROCEDURE calculateEmi(LoanAmount decimal(12,2),ROI FLOAT,Tenure NUMERIC(18,2))
BEGIN
DECLARE Tenure1 NUMERIC(18,2); 
DECLARE Period INT;
DECLARE Principal decimal(12,2);
DECLARE Interest decimal(12,2);
DECLARE EMI decimal(12,2);
DECLARE AMT decimal(12,2);

SET Tenure1=12*Tenure;			
SET Period = 1;
SET ROI = (ROI/100)/12;

DROP TABLE IF EXISTS EMI_TABLE;
CREATE TABLE EMI_TABLE(MONTHS INT,PAYMENT decimal(12,2),PRINCIPAL decimal(12,2),INTEREST decimal(12,2) ,BALANCE decimal(12,2));  


SET EMI = ROUND((LoanAmount * ROI * POWER((1 + ROI), Tenure1))  / (POWER((1 + ROI), Tenure1) - 1),2);    #EMI = [P x R x (1 + R)^N] / [(1 + R)^N - 1]

WHILE Period <= Tenure1
do
    	 
      SET Interest = ROUND((LoanAmount * ROI),2);   #INTEREST = [P x R]
      
    
      SET Principal = ROUND((EMI - Interest),2);    #PRINCIPAL = [EMI - INTEREST]
      
      
      SET LoanAmount = ROUND((LoanAmount - Principal),2);  #OUTSTANDING LOAN AMOUNT = [@LoanAmount - Principal]

      INSERT EMI_TABLE select Period,EMI,Principal,Interest,LoanAmount;

      SET Period = Period + 1;
      
END WHILE;

SELECT * FROM EMI_TABLE;
END
|
DELIMITER ; 

call calculateEmi(5000000.00,11,2.5);