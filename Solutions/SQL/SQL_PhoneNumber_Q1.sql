DROP FUNCTION IF EXISTS phoneNumber1;
SET GLOBAL log_bin_trust_function_creators = 1;

DELIMITER |

CREATE FUNCTION phoneNumber1(str varchar(50))
RETURNS varchar(50)

BEGIN 
	DECLARE phone varchar(50);
	DECLARE var1 varchar(50);
	DECLARE var2 varchar(50);
	DECLARE cnt int;
	DECLARE comp varchar(50);
	
	SET cnt=1;
	
	IF str NOT REGEXP '^[[:alnum:]]+$' OR LENGTH(str)!=10 
	THEN
		SET phone='Invalid Format';
	ELSE
		WHILE (cnt <= 10)	
		DO
			SET comp=SUBSTRING(str,cnt,1);
			SET var1=
			CASE
         WHEN comp IN ('a', 'b', 'c', '2') THEN 2
         WHEN comp IN ('d', 'e', 'f', '3') THEN 3
         WHEN comp IN ('g', 'h', 'i', '4') THEN 4
         WHEN comp IN ('j', 'k', 'l', '5') THEN 5
         WHEN comp IN ('m', 'n', 'o', '6') THEN 6
         WHEN comp IN ('p', 'q', 'r', 's', '7') THEN 7
         WHEN comp IN ('t', 'u', 'v', '8') THEN 8
         WHEN comp IN ('w', 'x', 'y', 'z', '9') THEN 9
         WHEN comp IN ('1') THEN 1
      	WHEN comp IN ('0') THEN 0
      	ELSE 0
			END;
			SET var2 =concat(ifnull(var2,''),var1);
			SET cnt = cnt+1;
			
		END WHILE;
			SET phone = CONCAT('(', SUBSTRING(var2, 1, 3), ')', ' ', SUBSTRING(var2, 4, 3), '-', SUBSTRING(var2, 7, 4));
	END IF;
	
RETURN phone;	
	
END |

DELIMITER ;

SELECT phoneNumber1('758ABC6586');