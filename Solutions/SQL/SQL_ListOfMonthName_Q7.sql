DROP PROCEDURE IF EXISTS monthDetails;

DELIMITER |

CREATE PROCEDURE monthDetails(date1 DATETIME) 
BEGIN
   
set @start_date = '2018-01-01';
set @end_date = date1;
set @months = -1;

select DATE_FORMAT(date_range,'%M') AS Month_Name from (
    select (date_add(@start_date, INTERVAL (@months := @months +1 ) month)) as date_range
    from mysql.help_topic a limit 0,1000) a
where a.date_range between @start_date and last_day(@end_date);


END;
| 
DELIMITER ;


CALL monthDetails('2018-09-01');