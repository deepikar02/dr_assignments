﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataClass;

namespace UserService.Controllers
{
    public class UserController : ApiController
    {
        [HttpGet]
        public IEnumerable<UserTable> GetUsers()
        {
            using (UserDB1Entities entities = new UserDB1Entities())
            {
                return entities.UserTables.ToList();
            }
        }

        [HttpGet]
        public UserTable GetUsersById(int id)
        {
            using (UserDB1Entities entities = new UserDB1Entities())
            {
                return entities.UserTables.FirstOrDefault(u => u.id == id);
            }
        }

        [HttpPost]
        public void RegisterUser([FromBody] UserTable user)
        {
            using (UserDB1Entities entities = new UserDB1Entities())
            {
                entities.UserTables.Add(user);
                entities.SaveChanges();
            }

        }

        [HttpDelete]
        public void DeleteUsers(int id)
        {
            using (UserDB1Entities entities = new UserDB1Entities())
            {
                entities.UserTables.Remove(entities.UserTables.FirstOrDefault(u => u.id == id));
                entities.SaveChanges();

            }

        }
        [HttpPut]
        public void UpdateUser(int id, [FromBody] UserTable user)
        {
            using (UserDB1Entities entities = new UserDB1Entities())
            {
                var entity = entities.UserTables.FirstOrDefault(u => u.id == id);

                entity.FirstName = user.FirstName;
                entity.LastName = user.LastName;
                entity.UserName = user.UserName;
                entity.Password = user.Password;
                entity.UserRole = user.UserRole;
                entity.DOB = user.DOB;
                entity.Address = user.Address;
                entity.City = user.City;
                entity.State = user.State;
                entity.Zip = user.Zip;
                entity.Email = user.Email;

                entities.SaveChanges();

            }

        }
    }
}
