﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CrudOperation
{
    public partial class CrudOperations : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }
        protected void lbInsert_Click(object sender, EventArgs e)
        {

            SqlDataSource1.InsertParameters["FirstName"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtFname")).Text;
            SqlDataSource1.InsertParameters["LastName"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtLname")).Text;
            SqlDataSource1.InsertParameters["UserName"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtUname")).Text;
            SqlDataSource1.InsertParameters["Password"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtPass")).Text;
            SqlDataSource1.InsertParameters["UserRole"].DefaultValue = ((DropDownList)GridView1.FooterRow.FindControl("DropDownList2")).SelectedValue;
            SqlDataSource1.InsertParameters["DOB"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtDOB")).Text;
            SqlDataSource1.InsertParameters["Address"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtAddress")).Text;
            SqlDataSource1.InsertParameters["City"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtCity")).Text;
            SqlDataSource1.InsertParameters["State"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtState")).Text;
            SqlDataSource1.InsertParameters["Zip"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtZip")).Text;
            SqlDataSource1.InsertParameters["Email"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtEmail")).Text;

            SqlDataSource1.Insert();
        }


    }
}